import click
from collections import defaultdict
import json
import numpy as np
import os
from pathlib import Path
import pandas as pd
from tqdm import tqdm
from typing import List
import shutil
import yaml


def make_dirs(dir="new_dir/"):
    """Creates a directory with subdirectories 'labels' and 'images', removing existing ones."""
    dir = Path(dir)
    if dir.exists():
        shutil.rmtree(dir)  # delete dir
    for p in dir, dir / "labels", dir / "images":
        p.mkdir(parents=True, exist_ok=True)  # make dir
    return dir


def coco91_to_coco80_class():  # converts 80-index (val2014) to 91-index (paper)
    """Converts COCO 91-class index (paper) to 80-class index (2014 challenge)."""
    return [
        0,
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        None,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        None,
        24,
        25,
        None,
        None,
        26,
        27,
        28,
        29,
        30,
        31,
        32,
        33,
        34,
        35,
        36,
        37,
        38,
        39,
        None,
        40,
        41,
        42,
        43,
        44,
        45,
        46,
        47,
        48,
        49,
        50,
        51,
        52,
        53,
        54,
        55,
        56,
        57,
        58,
        59,
        None,
        60,
        None,
        None,
        61,
        None,
        62,
        63,
        64,
        65,
        66,
        67,
        68,
        69,
        70,
        71,
        72,
        None,
        73,
        74,
        75,
        76,
        77,
        78,
        79,
        None,
    ]


def min_index(arr1, arr2):
    """
    Find a pair of indexes with the shortest distance.

    Args:
        arr1: (N, 2).
        arr2: (M, 2).
    Return:
        a pair of indexes(tuple).
    """
    dis = ((arr1[:, None, :] - arr2[None, :, :]) ** 2).sum(-1)
    return np.unravel_index(np.argmin(dis, axis=None), dis.shape)


def merge_multi_segment(segments):
    """
    Merge multi segments to one list. Find the coordinates with min distance between each segment, then connect these
    coordinates with one thin line to merge all segments into one.

    Args:
        segments(List(List)): original segmentations in coco's json file.
            like [segmentation1, segmentation2,...],
            each segmentation is a list of coordinates.
    """
    s = []
    segments = [np.array(i).reshape(-1, 2) for i in segments]
    idx_list = [[] for _ in range(len(segments))]

    # record the indexes with min distance between each segment
    for i in range(1, len(segments)):
        idx1, idx2 = min_index(segments[i - 1], segments[i])
        idx_list[i - 1].append(idx1)
        idx_list[i].append(idx2)

    # use two round to connect all the segments
    for k in range(2):
        # forward connection
        if k == 0:
            for i, idx in enumerate(idx_list):
                # middle segments have two indexes
                # reverse the index of middle segments
                if len(idx) == 2 and idx[0] > idx[1]:
                    idx = idx[::-1]
                    segments[i] = segments[i][::-1, :]

                segments[i] = np.roll(segments[i], -idx[0], axis=0)
                segments[i] = np.concatenate([segments[i], segments[i][:1]])
                # deal with the first segment and the last one
                if i in [0, len(idx_list) - 1]:
                    s.append(segments[i])
                else:
                    idx = [0, idx[1] - idx[0]]
                    s.append(segments[i][idx[0]: idx[1] + 1])

        else:
            for i in range(len(idx_list) - 1, -1, -1):
                if i not in [0, len(idx_list) - 1]:
                    idx = idx_list[i]
                    nidx = abs(idx[1] - idx[0])
                    s.append(segments[i][nidx:])
    return s


def convert_coco_json(json_dir="../coco/annotations/"):
    """Converts COCO JSON format to YOLO label format, with options for segments and class mapping."""
    save_dir = make_dirs()  # output directory

    # Import json
    for json_file in sorted(Path(json_dir).resolve().glob("*.json")):
        fn = Path(save_dir) / "labels" / json_file.stem.replace("instances_", "")  # folder name
        fn.mkdir()
        with open(json_file) as f:
            data = json.load(f)

        # Create image dict
        images = {"%g" % x["id"]: x for x in data["images"]}
        # Create image-annotations dict
        imgToAnns = defaultdict(list)
        for ann in data["annotations"]:
            imgToAnns[ann["image_id"]].append(ann)

        # Write labels file
        for img_id, anns in tqdm(imgToAnns.items(), desc=f"Annotations {json_file}"):
            img = images["%g" % img_id]
            h, w, f = img["height"], img["width"], img["file_name"].split('/')[1]

            bboxes = []
            for ann in anns:
                if ann["iscrowd"]:
                    continue
                # The COCO box format is [top left x, top left y, width, height]
                box = np.array(ann["bbox"], dtype=np.float64)
                box[:2] += box[2:] / 2  # xy top-left corner to center
                box[[0, 2]] /= w  # normalize x
                box[[1, 3]] /= h  # normalize y
                if box[2] <= 0 or box[3] <= 0:  # if w <= 0 and h <= 0
                    continue

                cls = ann["category_id"] - 1  # class
                box = [cls] + box.tolist()
                if box not in bboxes:
                    bboxes.append(box)

            # Write
            with open((fn / f).with_suffix(".txt"), "a") as file:
                for i in range(len(bboxes)):
                    line = (*(bboxes[i]),)  # cls, box
                    file.write(("%g " * len(line)).rstrip() % line + "\n")


def get_class_names(df: pd.DataFrame):
    cat_df = df[['category_id', 'category_name']]
    cat_df = cat_df.drop_duplicates()

    cat_df = cat_df.sort_values(by='category_id')

    class_names = [row['category_name'] for _, row in cat_df.iterrows()]

    return class_names


def save_config_yaml(
    output_data_path: str,
    train_path: str, test_path: str,
    class_names: List[str],
    cfg_file_name: str = 'data.yaml',
):
    cfg_path = os.path.join(output_data_path, cfg_file_name)

    info_to_save = {
        'path': output_data_path,
        'train': os.path.join(os.path.basename(train_path), 'images'),
        'val': os.path.join(os.path.basename(test_path), 'images'),
        'names': {
            k: v for k, v in zip(range(len(class_names)), class_names)
        }
    }

    with open(cfg_path, 'w') as f:
        yaml.dump(info_to_save, f, default_flow_style=False)


@click.command()
@click.argument('input_folder', type=click.Path(exists=True))
@click.argument('output_folder', type=click.Path(exists=False))
def main(
    input_folder: str, output_folder: str,
    tmp_dir: str = os.path.join('.', 'new_dir'),
):
    test_path = os.path.join(output_folder, 'test_annotation')
    train_path = os.path.join(output_folder, 'train_annotation')

    os.makedirs(train_path, exist_ok=True)
    os.makedirs(test_path, exist_ok=True)

    shutil.copy2(
        os.path.join(input_folder, 'train_anno.json'),
        os.path.join(train_path, 'train_anno.json')
    )
    shutil.copy2(
        os.path.join(input_folder, 'val_anno.json'),
        os.path.join(test_path, 'val_anno.json')
    )

    for folder in ['labels', 'images']:
        for path in [test_path, train_path]:
            os.makedirs(os.path.join(path, folder), exist_ok=True)

    convert_coco_json(train_path)
    for file in tqdm(os.listdir(os.path.join(tmp_dir, 'labels', 'train_anno'))):
        shutil.move(
            os.path.join(tmp_dir, 'labels', 'train_anno', file),
            os.path.join(train_path, 'labels', file)
        )

    convert_coco_json(test_path)
    for file in tqdm(os.listdir(os.path.join(tmp_dir, 'labels', 'val_anno'))):
        shutil.move(
            os.path.join(tmp_dir, 'labels', 'val_anno', file),
            os.path.join(test_path, 'labels', file)
        )

    shutil.rmtree(tmp_dir)

    test_labels = os.listdir(os.path.join(test_path, 'labels'))
    train_labels = os.listdir(os.path.join(train_path, 'labels'))

    test_labels = set(map(lambda x: x.split('.')[0], test_labels))
    train_labels = set(map(lambda x: x.split('.')[0], train_labels))

    images = os.path.join(input_folder, 'rtsd-frames', 'rtsd-frames')
    subset_df = pd.read_csv(
        os.path.join(input_folder, 'subset.csv')
    )

    for _, row in subset_df.iterrows():
        file_name = row['file_name'].split('/')[1]
        name = file_name.split('.')[0]

        img_path = os.path.join(images, file_name)

        if name in train_labels:
            shutil.copy2(img_path, os.path.join(train_path, 'images', file_name))
        elif name in test_labels:
            shutil.copy2(img_path, os.path.join(test_path, 'images', file_name))

    class_names = get_class_names(subset_df)
    save_config_yaml(output_folder, train_path, test_path, class_names)


if __name__ == '__main__':
    main()
