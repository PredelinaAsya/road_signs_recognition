import cv2
import os
import yaml

from src.models.predict_model import get_model_preds


def read_class_mapping(
        class_names_yaml: str,
):
    with open(class_names_yaml, 'r') as f:
        yaml_dict = yaml.safe_load(f)

    class_mapping = yaml_dict['names']

    return class_mapping


def render_res_on_video(
        model,
        input_video_path: str,
        out_videos_folder: str,
        class_names_yaml: str,
        conf_thr: float = 0.5,
):
    class_mapping = read_class_mapping(class_names_yaml)

    source = cv2.VideoCapture(input_video_path)
    video_label = os.path.basename(
        input_video_path
    ).split('.')[0]

    out_path = os.path.join(
        out_videos_folder, f'{video_label}.mp4',
    )

    width = int(source.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(source.get(cv2.CAP_PROP_FRAME_HEIGHT))

    out_stream = cv2.VideoWriter(
        out_path, cv2.VideoWriter_fourcc(*'h256'),
        source.get(cv2.CAP_PROP_FPS), (width, height),
    )

    frame_count = 0

    while True:
        ret, frame = source.read()

        if not ret:
            break

        # Run YOLOv8 inference on the frame
        result = get_model_preds(model, frame, conf=conf_thr)

        for cls_id, custom_label in class_mapping.items():
            if cls_id in result.names:  # check if the class id is in the results
                result.names[cls_id] = custom_label  # replace the class name with the custom label

        # Visualize the results on the frame
        rendered_frame = result.plot()

        out_stream.write(rendered_frame)
        frame_count += 1

    return out_path
