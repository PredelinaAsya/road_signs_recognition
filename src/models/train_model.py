import click
from ultralytics import YOLO, settings
from dotenv import load_dotenv
import mlflow
from mlflow.pyfunc import PythonModel
import cv2

load_dotenv()

# mlflow.set_experiment("Default")  # change if needed

settings.update({"mlflow": False, "wandb": False})


class UltralyticsYoloModel(PythonModel):
    def __init__(self):
        self.model = None

    def load_context(self, context):
        model_file_path = context.artifacts["model_file"]
        model_file_path = model_file_path.replace('\\', '/')
        print(f"Model file path: {model_file_path}")
        self.model = YOLO(model_file_path)

    def predict(self, context, img_path):
        if self.model is None:
            raise ValueError(
                "The model has not been loaded. "
                "Ensure that 'load_context' is properly executed."
            )

        frame = cv2.imread(img_path)
        return self.model.predict(frame, conf=0.5)


def train_detector(
        data_cfg_path: str,
        model_type: str = 'yolov8m',
        epochs: int = 1,
        batch_size: int = -1,
        save_period: int = 1,
        seed: int = 42,
):
    with mlflow.start_run() as run:
        model = YOLO(f'{model_type}.pt')
        results = model.train(
            data=data_cfg_path,
            epochs=epochs,
            batch=batch_size,
            save_period=save_period,
            seed=seed
        )

        print('starting to log params...')
        mlflow.log_params({
            "model_type": model_type,
            "epochs": epochs,
            "batch_size": batch_size,
            "save_period": save_period,
            "seed": seed,
        })

        print('starting to log metrics...')
        metrics = {k.replace('(', '_').replace(')', ''): v for k, v in results.results_dict.items()}
        mlflow.log_metrics(metrics)

        print('starting to log model...')
        yolo_mlflow_model = UltralyticsYoloModel()
        model_file_path = str(results.save_dir / 'weights' / 'best.pt')
        model_info = mlflow.pyfunc.log_model(
            artifact_path="yolov8m-rstd",
            python_model=yolo_mlflow_model,
            artifacts={"model_file": model_file_path},
            pip_requirements=[
                "ultralytics",
                "cv2"
            ],
        )


@click.command()
@click.argument('dataset_config_path', type=click.Path(exists=True))
@click.option('--model_type', default='yolov8m')
@click.option('--epochs', default=1)
@click.option('--batch_size', default=-1)
@click.option('--save_period', default=1)
@click.option('--seed', default=42)
def main(dataset_config_path: str, model_type: str, epochs: int, batch_size: int, save_period: int, seed: int):
    train_detector(dataset_config_path, model_type, epochs, batch_size, save_period, seed)


if __name__ == '__main__':
    # train_detector('..\..\data\processed\processed_min_subset_8_imgs\data.yaml')  # for testing
    main()
