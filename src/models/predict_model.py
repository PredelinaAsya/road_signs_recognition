def get_model_preds(
    model, frame,
    conf_thr: float = 0.4,
):
    result = model.predict(
        frame, conf=conf_thr, verbose=False
    )[0]
    return result
